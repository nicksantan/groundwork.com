<!DOCTYPE html>

<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->

<html <?php language_attributes(); ?>>

<head>
	
<title>
	<?php wp_title( '-', true, 'right' ); ?>
</title>

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<!-- <meta name="viewport" content="width=1300" /> -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<header id="page-header">
  <div class="page-nav-placeholder"></div>

  <nav class="page-nav">
    <ul>
      <li><div class = "grid-button-container"><img src = "/wp-content/themes/groundwork/images/x-blue.svg"></div></li>
      <li>
        <a href="/" class="logo"><?php echo file_get_contents(get_stylesheet_directory()."/images/groundwork_logo_nav.svg"); ?></a>
      </li>
      <li><a href="#" class="about">About</a></li>
    </ul>
  </nav>

  <nav class="page-sub-nav">
    <ul>
      <li>
        <a href="#about-tetra-pak-tab" data-toggle="tab">
          <span>ABOUT TETRA PAK</span>
        </a>
      </li>
      <li>
        <a href="#about-groundwork-tab" data-toggle="tab">
          <span>ABOUT GROUNDWORK</span>
        </a>
      </li>
    </ul>
  </nav>

  <div class="grid-nav-content">
    <div class="grid-nav-content-inner-background">
      <div class="mobile-only-global mobile-grid-nav-type">PROJECT</div>
      <ul class="grid-nav-content-inner">
        <?php
          wp_reset_query();
          $args = array(
            'posts_per_page' => 6,
            'order' => 'ASC'
          );
          $query = new WP_Query( $args );
          $nav_posts = $query->posts;
          $grid_number = 1;
        ?>
        <?php foreach ( $nav_posts as $post ) : ?>
          <li class="grid-nav-item">
            <div class="hover-background mobile-hidden"></div>

            <a href="<?php the_permalink(); ?>" class="grid-nav-item-link">
              <div class="grid-nav-item-image-mask mobile-hidden">
                <?php echo file_get_contents(get_stylesheet_directory()."/images/nav_grid_image_mask.svg"); ?>
              </div>
              <div class="grid-nav-item-image" style="background-image: url('<?php echo get_field('hero_image'); ?>')">
                <div class="over-image-line"></div>
              </div>
              <span class="grid-nav-item-type mobile-hidden">Project</span>
              <h3><?php the_title(); ?></h3>
              <div class="grid-nav-item-number-border mobile-hidden"></div>
              <span class="grid-nav-item-number">0<?php echo $grid_number; ?></span>
              <div class="grid-nav-item-border mobile-hidden"></div>
            </a>
          </li>
          <?php $grid_number += 1; ?>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</header>

<div class="about-tab-content">
  <div role="tabpanel" class="tab-pane" id="about-tetra-pak-tab"><?php get_template_part("page-about-tetra-pak") ?></div>
  <div role="tabpanel" class="tab-pane" id="about-groundwork-tab"><?php get_template_part("page-about-groundwork") ?></div>
</div>

