<?php

// Debugging 

$debugging = false;
if ( SERVER_ENVIRONMENT === 'dev' ) $debugging = true;


//
// Stylesheets and Javascript
//

function groundwork_scripts_styles() {

    $version_number = '0.1';
	global $debugging;

    // Theme registration stylesheet, combined theme stylesheet and any other necessary styles
    wp_enqueue_style( 'registration-stylesheet', get_stylesheet_directory_uri() . '/style.css', $version_number );
    wp_enqueue_style( 'theme-styles', get_stylesheet_directory_uri() . '/css/theme.css', $version_number );

    // Use non-minified JS files for local dev, otherwise load single concatinated app.js
	if( $debugging )  {
        wp_enqueue_script( 'bootstrap-transition', get_stylesheet_directory_uri() . '/js/bootstrap/transition.js', array( 'jquery' ), $version_number, true );
        wp_enqueue_script( 'bootstrap-carousel', get_stylesheet_directory_uri() . '/js/bootstrap/carousel.js', array( 'jquery' ), $version_number, true );
        wp_enqueue_script( 'jquery-bc-swipe', get_stylesheet_directory_uri() . '/js/bootstrap/jquery.bcSwipe.js', array( 'jquery' ), $version_number, true );
        wp_enqueue_script( 'bootstrap-tab', get_stylesheet_directory_uri() . '/js/bootstrap/tab.js', array( 'jquery' ), $version_number, true );
        wp_enqueue_script( 'bootstrap-modal', get_stylesheet_directory_uri() . '/js/bootstrap/modal.js', array( 'jquery' ), $version_number, true );
        wp_enqueue_script( 'remodal', get_stylesheet_directory_uri() . '/js/remodal.min.js', array( 'jquery' ), $version_number, true );
        wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/js/slick.min.js', array( 'jquery' ), $version_number, true );
        wp_enqueue_script( 'waypoint', get_stylesheet_directory_uri() . '/js/jquery.waypoints.js', array( 'jquery' ), $version_number, true );
        wp_enqueue_script( 'd3', get_stylesheet_directory_uri() . '/js/d3_v3.js', array( 'jquery' ), $version_number, true );
        wp_enqueue_script( 'theme-scripts', get_stylesheet_directory_uri() . '/js/interaction.js', array( 'jquery' ), $version_number, true );
        // ... add more scripts as you use them

	} 
	else {

    	wp_enqueue_script( 'all-js', get_stylesheet_directory_uri() . '/js/app.js', array( 'jquery' ), $version_number, true );

    }

    // Only load (beefy) visualization scripts

}

add_action( 'wp_enqueue_scripts', 'groundwork_scripts_styles' );

function theme_setup() {

    // Adds RSS feed links to <head> for posts and comments.
    add_theme_support( 'automatic-feed-links' );

    // This theme uses wp_nav_menu() some locations
    register_nav_menus(
        array(
            'main-menu' => __( 'Main Menu' ),
        )
    );

    //  Enable featured images for posts
    add_theme_support( 'post-thumbnails' );

    // Replaces the excerpt "more" text
    // function new_excerpt_more($more) {
    //     global $post;
    //     return '<a class="more-tag" href="'. get_permalink($post->ID) . '">Read more</a>';
    // }
    // add_filter('excerpt_more', 'new_excerpt_more');

}

add_action( 'after_setup_theme', 'theme_setup' );
// Function to change "posts" to "stories" in the admin side menu
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Stories';
    $submenu['edit.php'][5][0] = 'Stories';
    $submenu['edit.php'][10][0] = 'Add Story';
    $submenu['edit.php'][16][0] = 'Tags';
    echo '';
}
add_action( 'admin_menu', 'change_post_menu_label' );
// Function to change post object labels to "news"
function change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Story';
    $labels->singular_name = 'Story';
    $labels->add_new = 'Add Story';
    $labels->add_new_item = 'Add Story';
    $labels->edit_item = 'Edit Story';
    $labels->new_item = 'Story';
    $labels->view_item = 'View Story';
    $labels->search_items = 'Search Stories';
    $labels->not_found = 'No Stories found';
    $labels->not_found_in_trash = 'No Stories found in Trash';
}
add_action( 'init', 'change_post_object_label' );



// Register shortcodes for use in WYSIWYG editor
function explore_link_shortcode($atts, $content, $name){
    $values = shortcode_atts(array(
        'link' => '#'
    ),$atts);

    $output = '<a class = "explore-link-module" href = "' . esc_url($values['link']) . '"> <span class = "explore-text">' . $content . '</span><span class = "explore-link">Explore<img src = "/wp-content/themes/groundwork/images/explore_arrow.svg"></span></a>';

    return $output; 
}

add_shortcode('explore_link','explore_link_shortcode');

function learn_more_link_shortcode($atts, $content, $name){
    $values = shortcode_atts(array(
        'link' => '#'
    ),$atts);

    $output = '<a class = "explore-text" href = "' . esc_url($values['link']) . '">' . $content . ' Learn more <img src = "/wp-content/themes/groundwork/images/explore_arrow.svg"></a>';

    return $output; 
}

add_shortcode('learn_more_link','learn_more_link_shortcode');

function mini_share_module_shortcode($atts, $content, $name){
    $output = '<div class = "mini-share-module"><div class = "social-links"><a class = "fb share social-icon" href = "#"></a><a class = "twitter share social-icon" href = "#"></a></div></div>';
    return $output; 
}

add_shortcode('mini_share_module','mini_share_module_shortcode');

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

// Custom WP Walker
@include 'utilities/walker.php';