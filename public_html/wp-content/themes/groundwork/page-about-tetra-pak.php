<div class="about-container about-tetra-pak-container">
	<section class="module-text module-text-pre-founder-quote">
		<div class="inner-container inner-container-with-side-items">
			<div class="tetrapak-logo tetrapak-logo-white"></div>

			<div class="social-icons">
				<div class="border"></div>
			<!-- 	<a class="fb fb-white social-icon" href="#"></a>
				<a class="twitter twitter-white social-icon" href="#"></a> -->
			</div>

			<h1>A package should save<br>more than it costs.</h1>

			<p>It all started in the 1940s, with the search for a better way to package milk.</p>

			<p>Something that could protect what was inside—and the people who drank it—by keeping it safe and stable, even when refrigeration wasn’t available. Something efficient, with a minimized impact on the environment.</p>
		</div>
	</section>

	<section class="module-blockquote-with-image">
		<div class="inner-container">
			<div class="row">
				<blockquote class="col-6 sm-col-12 tablet-col-9 tablet-push-1 mobile-col-12 tablet-push-0">
					<p>“Doing something that nobody else had done before is actually quite hard.”</p>
				</blockquote>

				<div class="col-6 sm-col-12 tablet-col-12 blockquote-image-container">
					<div class="image-title">
						<span>DR. RUBEN RAUSING</span>
						<div class="mini-divider"></div>
						<span class="image-caption">Founder of Tetra Pak</span>
					</div>
					<div class="blockquote-image" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/about_tetra_pak_quote.png')"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="module-text">
		<div class="inner-container">
			<p>What we set out to do had never been done, and it took a decade of development to create the first paper-based package that could do what we had in mind. Even the way we planned to fill it (and keep it safe, healthy, and free of bacteria in the process) presented a puzzle that eventually became one of our hallmarks.</p>
		</div>
	</section>

	<section class="module-blockquote-centered">
		<div class="inner-container">
			<blockquote>
				<p>Our aseptic cartons were considered one of the most important food innovations of the 20th century.</p>
			</blockquote>
		</div>
	</section>

	<section class="module-text">
		<div class="inner-container">
			<p>It was an enormous challenge. But it’s how we created the first Tetra Pak carton package, the distinctive tetrahedron-shaped packaging that inspired our name with its simple, efficient design.</p>
		</div>
	</section>

	<section class="about-tetra-pak-packaging-images"></section>

	<section class="module-text">
		<div class="inner-container">
			<p>For over half a century, we’ve been creating carton packaging that can safely and sustainably hold liquid food—including milk, of course—to meet the needs of hundreds of millions of people every day.</p>

			<p>Today, we’re able to get food to people everywhere, protecting them by protecting what’s inside, with only minimal impact on our environment.</p>
		</div>
	</section>

	<section class="module-blockquote-centered module-blockquote-centered-very-small">
		<div class="inner-container">
			<blockquote>
				<p>At Tetra Pak, we protect what’s good.</p>
			</blockquote>
		</div>
	</section>

	<section class="module-text">
		<div class="inner-container inner-container-with-side-items">
			<p>We still abide by our founding philosophy, the idea that packaging should save more—food and resources—than it costs. It's a way of thought that matters even more today than when we started <br>out—and one which will matter even more tomorrow.</p>
			<div class="module-text__contact">
				<a href="mailto:info.us@tetrapak.com" target="_top">CONTACT US</a>
			</div>
		</div>

	</section>
</div>
