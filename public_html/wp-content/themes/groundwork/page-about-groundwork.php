<div class="about-container about-groundwork-container">
	<section class="module-text">
		<div class="inner-container inner-container-with-side-items">
			<div class="tetrahedron-graphic"></div>

			<div class="social-icons">
				<div class="border"></div>
				<!-- <a class="fb social-icon" href="#"></a>
				<a class="twitter social-icon" href="#"></a> -->
			</div>

			<h1>Tetra Pak isn’t the only one protecting what’s good.</h1>

			<p>All over the world, people are hard at work in small ways on the things that matter to all of us most, from the environment around us to the food that fuels us.</p>

			<p>They’re changing how we raise, consume, and think about food, how we care for ourselves and our resources, and they’re sharing the kind of ideas that will change our future for the better.</p>

			<p>They’re doing it at the grassroots, quietly and without fanfare.</p>
		</div>
	</section>

	<section class="module-blockquote-centered">
		<div class="inner-container">
			<blockquote>
				<p>We’re sharing their stories.</p>
			</blockquote>
		</div>
	</section>

	<section class="module-text">
		<div class="inner-container">
			<p>At Groundwork, we’re giving them the attention they deserve: yours. We’re sharing their stories so you can, too.</p>

			<p>It’s just one small way you can make an impact. Because when it comes to innovation, inspiration, and changing the world, sometimes one good idea, shared, is all it takes.</p>
			<p class="strong"><strong>Spread the word.</strong></p>

			<p><i>Know someone who’s doing their part to change our world from the ground up?</i></p>
			<p><a class="link-with-arrow" href="mailto:info@groundworkpresents.com">Give our editors the heads up</a></p>
		</div>
	</section>

</div>
