<?php get_header(); ?>

<div class="djax-content" id="<?php echo $post->post_name; ?>">


		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


		<section class = "title-card" style = "background-image: url('<?php echo esc_url(the_field('hero_image'));?>')">
			<div class = "mobile-hero" style = "background-image: url('<?php echo esc_url(the_field('hero_image'));?>')">
			</div>
			<div class = "content-card">
				<h3><?php the_field('category'); ?></h2>
				<h1><?php the_title(); ?></h1>
				<h2><?php the_field('summary'); ?></h2>
				<div class = "row">
				<div class = "factoid-container col-9 push-3">
					<div class = "row">
						<div class = "col-4">
							<h5><?php the_field('factoid_1_label'); ?></h5>
							<span><?php the_field('factoid_1'); ?></span>
						</div>
						<div class = "col-4">
							<h5><?php the_field('factoid_2_label'); ?></h5>
							<span><?php the_field('factoid_2'); ?></span>
						</div>
						<div class = "col-4">
							<h5><?php the_field('factoid_3_label'); ?></h5>
							<span><?php the_field('factoid_3'); ?></span>
						</div>
					</div>
					<div class = "row">
						<div class = "col-4">
							<h5>Share:</h5>
							<a class = "fb share social-icon" href = "#"></a><a class = "twitter share social-icon" href = "#"></a>
						</div>
						<div class = "col-8">
							<h5>Brought to you by:</h5>
							<a class = "tetrapak-logo about" href = "#"></a>
						</div>
				</div>
			</div>
			</div>
		</section>


<?php
// Start flexible content
if ( have_rows('sections') ) :

	$index = 1;

	while ( have_rows('sections') ) : the_row();

		switch ( get_row_layout() ) {

			case 'text_block': ?>
				<section class="module-text">
					<div class = "inner-container">	
						
							<?php the_sub_field('content_text'); ?>
						

					</div>
				</section>

			<?php break;

			case 'blank_section': ?>
			<section class="module-text">
				<div class="inner-container html-block full">
					<?php the_sub_field('content_text'); ?>
				</div>
			</section>

			<?php break;

			case 'interactive_piece': ?>
				<section class="module-text">
					<div class="inner-container html-block interactive-pieces">
						<div class="row">
							<div class="col-4 gauge">

								<div class="container">
									<div class="progress one">
									    <div class="progress__bar"></div>
									    <div id="year">1997</div>
								  	</div>
								  	<div class="progress two">
									    <div class="progress__bar"></div>
									    <div id="year">2011</div>
								  	</div>
								  	<div class="html-block__description">
										<p>Food Allergies among children increased</p>
										<h3>APPROXIMATELY 50%</h3>
										<p>between</p>
										<h3>1997 and 2011.</h3>
								  	</div>
								 </div>
								
							</div>
							<div class="col-4 clock">
								
								<div class="container">
									<div class="clocks">
										<div class="clock-container">
											<?php include('interactions/clock.svg'); ?>
										</div>
									</div>
									<div class="html-block__description">
										<p>Every</p>
										<h3>3 minutes</h3>
										<p>a food allergy reaction sends someone to the
										emergency room.</p>
								  	</div>
							  	</div>
							</div>
							<div class="col-4 map">

								<div class="container">
									<div class="map-container">
										<?php include('interactions/map.svg'); ?>
										<h3><span>Up to</span>15 Million</h3>
									</div>
					
									<div class="html-block__description">
										<p>Americans have food allergies.</p>
								  	</div>
							  	</div>
								
							</div>
						</div>
						<div class="row second">
							<div class="col-6 push-1 octagauge">
								<div class="octagauge__container">
									<?php include('interactions/octa.svg'); ?>
									<div class="progress-circle-container">
									<?php include('interactions/progress-circle.svg'); ?>
									<div class="progress-text" data-progress="0">0%</div>
									</div>				
								</div>
								
								
							</div>
							<div class="col-3 report">
								<div class="html-block__description report__container">
									<h3>8 Foods</h3>
									<p>account for</p>
									<h3>90% of all</h3>
									<p>reactions.</p>
							  	</div>
							</div>
						</div>
					</div>			
				</section>

			<?php break;

			case 'drop_visualization': ?>
				<section class="module-text">
					<div class="inner-container html-block water-drop">
						<div class="row menu">
							<div class="col-4 domestic">
								<h5>Domestic consumption</h5>
							</div>
							<!-- Domestic mobile asset -->
							<div class="mobile-only">
								<img class="mobile-only__water" src="<?php echo get_bloginfo('template_directory'); ?>/images/water/mobile/RenewableLiving_mobile_infographics2_2.png" alt="water_gallons">
								<div class="mobile-only__description">
									<h2>36 gallons</h2>
									<p>Showers, laundry, cooking, and flushing all contribute to our  domestic consumption.</p>	
								</div>
							</div>
								
							<div class="col-4 industrial">
								<h5>Industrial production</h5>
							</div>
							<!-- Industrial mobile asset -->
							<div class="mobile-only">
								<img class="mobile-only__water" src="<?php echo get_bloginfo('template_directory'); ?>/images/water/mobile/RenewableLiving_mobile_infographics2_3.png" alt="water_gallons">
								<div class="mobile-only__description">
									<h2>44 gallons</h2>
									<p>Factories depend on water to make and clean everyday materials, like plastic, metal, and cotton.</p>
								</div>
							</div>
							<div class="col-4 food">
								<h5>Food production</h5>
							</div>
							<!--  Food asset -->
							<div class="mobile-only">
								<img class="mobile-only__water" src="<?php echo get_bloginfo('template_directory'); ?>/images/water/mobile/RenewableLiving_mobile_infographics2_4.png" alt="water_gallons">
								<div class="mobile-only__description">
									<h2>923 gallons</h2>
									<p>Most of the water we consume goes into raising and transporting what we eat and drink.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">

								<div class="image-container">
									<img class="image-container__water" src="<?php echo get_bloginfo('template_directory'); ?>/images/water/water_01.png" alt="water_gallons">
									<div class="image-container__description-1">
										<h2>36 gallons</h2>
										<p>Showers, laundry, cooking, and flushing all contribute to our  domestic consumption.</p>
									</div>
									<div class="image-container__description-2">
										<h2>44 gallons</h2>
										<p>Factories depend on water to make and clean everyday materials, like plastic, metal, and cotton.</p>
									</div>
									<div class="image-container__description-3">
										<h2>923 gallons</h2>
										<p>Most of the water we consume goes into raising and transporting what we eat and drink.</p>
									</div>
								</div>
							

							</div>
						</div>		

					</div>
				</section>

			<?php break;

			case 'line_graph': ?>
				<section class="module-text">
					<div class = "inner-container html-block line-graph">	
						<div class="row menu">
							<div class="col-4" data="fossil">
								<h5>Fossil fuels</h5>
								
							</div>

							<div class="line-graph__mobile--container">
								<div class="row">
									<div class="col-4 line-graph__mobile">
										<h5>COAL</h5>
										<div class="line-graph__mobile-stat">
											<div>
												<span>38</span><span>yrs</span>
											</div>
										</div>
									</div>
									<div class="col-4 line-graph__mobile">
										<h5>OIL</h5>
										<div class="line-graph__mobile-stat">
											<div>
												<span>33</span><span>yrs</span>
											</div>
										</div>
									</div>
									<div class="col-4 line-graph__mobile">
										<h5>GAS</h5>
										<div class="line-graph__mobile-stat">
											<div>
												<span>31</span><span>yrs</span>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-4" data="minerals">
								<h5>Minerals</h5>
							</div>

							<div class="line-graph__mobile--container">
								<div class="row">
									<div class="col-4 line-graph__mobile">
										<h5>ALUMINUM</h5>
										<div class="line-graph__mobile-stat">
											<div>
												<span>76</span><span>yrs</span>
											</div>
										</div>
									</div>
									<div class="col-4 line-graph__mobile">
										<h5>COPPER</h5>
										<div class="line-graph__mobile-stat">
											<div>
												<span>28</span><span>yrs</span>
											</div>
										</div>
									</div>
									<div class="col-4 line-graph__mobile">
										<h5>SILVER</h5>
										<div class="line-graph__mobile-stat">
											<div>
												<span>13</span><span>yrs</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-4" data="ecosystems">
								<h5>Ecosystems</h5>
							</div>

							<div class="line-graph__mobile--container">
								<div class="row">
									<div class="col-4 line-graph__mobile">
										<h5>RAINFOREST</h5>
										<div class="line-graph__mobile-stat">
											<div>
												<span>192</span><span>yrs</span>
											</div>
										</div>
									</div>
									<div class="col-4 line-graph__mobile">
										<h5>COAL REEFS</h5>
										<div class="line-graph__mobile-stat">
											<div>
												<span>84</span><span>yrs</span>
											</div>
										</div>
									</div>
									<div class="col-4 line-graph__mobile">
										<h5>FARMLAND</h5>
										<div class="line-graph__mobile-stat">
											<div>
												<span>65</span><span>yrs</span>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col-12">	
								<div id="chart"></div>
							</div>
						</div>
					</div>
				</section>

			<?php break;

			case 'text_block_with_background': ?>
				<section class="module-text-with-background" style = "background-image: url('<?php echo esc_url(the_sub_field('background_image'));?>')">
					<div class = "inner-container">	
						<p>
							<?php the_sub_field('content_text'); ?>
						</p>
					</div>
				</section>
			<?php break;

			case 'text_block_with_two_images': ?>
				<section class="module-text-with-two-images">
					<div class = "left-image" style = "background-image: url('<?php echo esc_url(the_sub_field('left_image'));?>')"></div>
					<div class = "inner-container">	
							<?php the_sub_field('content_text'); ?>
					</div>
					<div class = "right-image" style = "background-image: url('<?php echo esc_url(the_sub_field('right_image'));?>')"></div>
				</section>
			<?php break;

			case 'double_image': ?>
				<section class="module-double-image">
					<div class = "inner-container">	
						<div class = "image-one" style = "background-image: url('<?php echo esc_url(the_sub_field('image_one'));?>')"></div>
						<div class = "image-two" style = "background-image: url('<?php echo esc_url(the_sub_field('image_two'));?>')"></div>

						<?php the_sub_field('content_text'); ?></h3>
					</div>
				</section>
			<?php break;

			case 'blockquote_centered': ?>
				<section class = "module-blockquote-centered">
					<div class = "inner-container">
						<blockquote>
							<?php the_sub_field('content_text'); ?>
						</blockquote>
					<div>
				</section>
			<?php break;

			case 'blockquote_with_image': ?>
				<section class = "module-blockquote-with-image">
					<div class = "inner-container large">
						<div class = "row">
						<blockquote class = "col-6 sm-col-12">
							<?php the_sub_field('content_text'); ?>
						</blockquote>
				
						<div class = "col-6 sm-col-12 blockquote-image-container">
							<?php if (get_sub_field('image_title')){ ?>
								<div class = "image-title">
									<span><?php the_sub_field('image_title'); ?></span>
									<!-- DIVIDING LINE HERE -->
									<div class = "mini-divider"></div>
									<span class = "image-caption"><?php the_sub_field('image_caption'); ?></span>
								</div>
							<?php } ?>
							
							<div class = "blockquote-image" style = "background-image: url('<?php echo esc_url(the_sub_field('blockquote_image'));?>')">
						</div>
					</div>
					</div>	
				</section>
			<?php break;


			case 'timeline': ?>
				<section class = "module-timeline">
					<div class = "inner-container large">
						<h3><?php the_sub_field('timeline_category'); ?></h5>
						<h2><?php the_sub_field('timeline_title'); ?></h2>
						<div class = "mini-divider"></div>
						<?php the_sub_field('timeline_description'); ?>
					</div>
					<div class="timeline-container">
						<?php while(has_sub_field('timeline_items')): ?>
						<div class = "timeline-item">
							<div class = "timeline-image" style = "background-image: url('<?php echo esc_url(the_sub_field('small_image'));?>')"></div>
							<div class = "timeline-info-container">
								<h4><?php the_sub_field('item_title'); ?></h4>
								<div class = "mini-divider"></div>
								<p><?php the_sub_field('item_description'); ?></p>
								<h5>Location:</h5>
								<span class = "timeline-location"><?php the_sub_field('item_location'); ?></span>
								<h5><?php the_sub_field('item_extinction_label'); ?></h5>
								<span><?php the_sub_field('item_year_of_extinction'); ?></span>
							</div>
							<a class = "expand-timeline-item-button" href = "#<?php the_sub_field('item_title'); ?>"></a>
						</div>
						<div class="remodal" data-remodal-id="<?php the_sub_field('item_title'); ?>">
								<button data-remodal-action="close" class="remodal-close"></button>
								<div class = "large-ingredient" style = "background-image: url('<?php echo esc_url(the_sub_field('large_image'));?>')"></div>
							<div class = "large-info-pane">
								<h4><?php the_sub_field('item_title'); ?></h4>
								<div class = "mini-divider"></div>
								<p class = "full-description"><?php the_sub_field('full_item_description'); ?></p>
								<div class = "supplementary-info-container">
									<h5>Location:</h5>
									<span class = "timeline-location"><?php the_sub_field('item_location'); ?></span>
									<h5><?php the_sub_field('item_extinction_label'); ?></h5>
									<span><?php the_sub_field('item_year_of_extinction');?></span>
									<h5>Source:</h5>
									<span><?php the_sub_field('item_source');?></span> 
								</div>

								<div class = "region-image-container" style = "background-image: url('<?php echo esc_url(the_sub_field('item_region_image'));?>')"></div>
							</div>
							<div class = "supplementary-info-container-tablet">
								<div>
									<h5>Location:</h5>
									<span class = "timeline-location"><?php the_sub_field('item_location'); ?></span>
								</div>
								<div>
									<h5><?php the_sub_field('item_extinction_label'); ?></h5>
									<span><?php the_sub_field('item_year_of_extinction');?></span>
								</div>
								<div>
									<h5>Source:</h5>
									<span><?php the_sub_field('item_source');?></span> 
								</div>
							</div>
						</div>
						<?php endwhile; ?>
					</div>
					<div class = "pagination-tray">
						<div class = "previous-button"></div>
						<div class = "next-button"></div>
					</div>
				</section>
			<?php break;
				case 'blank_timeline': ?>
				<section class = "module-timeline blank">
					<div class = "inner-container large">
						<h3><?php the_sub_field('timeline_category'); ?></h5>
						<h2><?php the_sub_field('timeline_title'); ?></h2>
						<div class = "mini-divider"></div>
						<?php the_sub_field('timeline_description'); ?>
					</div>
					<div class="timeline-container">
						<?php while(has_sub_field('timeline_items')): ?>
						<div class = "timeline-item">
							<div class = "timeline-image" style = "background-image: url('<?php echo esc_url(the_sub_field('small_image'));?>')"></div>
							<div class = "timeline-info-container">
								<h4><?php the_sub_field('item_title'); ?></h4>
								<div class = "mini-divider"></div>
								<p><?php the_sub_field('item_description'); ?><p>
							</div>
						</div>
						<?php endwhile; ?>
					</div>
					<div class = "pagination-tray">
						<div class = "previous-button"></div>
						<div class = "next-button"></div>
					</div>
				</section>
			<?php break;
			case 'featured_exhibit' : ?>
			<section class="featured-exhibit">
				<div class = "inner-container large">
					<h3><?php the_sub_field('featured_exhibit_category'); ?></h5>
					<h2><?php the_sub_field('featured_exhibit_title'); ?></h2>
					<div class = "mini-divider"></div>
					<?php the_sub_field('featured_exhibit_description'); ?>
				</div>
			</section>
			<?php break;
			case 'interactive_map': ?>
				<section class="module-map featured-exhibit">
					<div class = "inner-container large">
						<h3>Featured Exhibit<h3>
						<h2>What in the World is in the Vault?</h2>
						<div class = "mini-divider"></div>
						<p>The Svalbard Global Seed Vault contains seeds from all over the world. Click on the foods below to see which countries deposited the seeds into the vault.</p>
						<p><i>Source</i>: <a target = "_blank" href = "http://www.nordgen.org/sgsv/">NordGen</a></p>
					</div>
					<ul class="food-items">
						<?php
							$food_json_data = file_get_contents(get_stylesheet_directory()."/map-data.json");
							$food_data = json_decode($food_json_data);
						?>
							<?php foreach ($food_data as $food_item) { ?>
							<li>
								<span class="module-map-food-name" data-countries="<?php foreach ( $food_item->countries as $country ) { echo $country.' '; } ?>" data-description="<?php echo $food_item->description ?>" data-image="<?php echo get_stylesheet_directory_uri() ?>/images/map_images/<?php echo $food_item->image_name ?>">
									<?php echo $food_item->food_name ?>
								</span>
							</li>
						<?php } ?>
					</ul>
					<?php echo file_get_contents(get_stylesheet_directory()."/images/map.svg"); ?>

					<div id="map-modal" class="modal fade" tabindex="-1" role="dialog">
						<div class="modal-dialog">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
							<div class="map-modal-image"></div>
							<h3></h3>
							<div class="map-modal-mini-divider"></div>
							<p class="map-modal-description"></p>
							<span class="map-modal-location-label">Location:</span>
							<p class="map-modal-countries"></p>
						</div>
					</div>
				</section>
				<section class = "module-timeline blank mobile-only">
					<div class = "inner-container large">
						<h3>Featured Exhibit</h5>
						<h2>What in the World is in the Vault?</h2>
						<div class = "mini-divider"></div>
						<p>The Svalbard Global Seed Vault contains seeds from all over the world. Click on the foods below to see which countries deposited the seeds into the vault.</p>
						<p><i>Source</i>: <a target = "_blank" href = "http://www.nordgen.org/sgsv/">NordGen</a></p>
					</div>
					<div class="timeline-container">
						<?php
						$food_json_data = file_get_contents(get_stylesheet_directory()."/map-data.json");
						$food_data = json_decode($food_json_data);
						?>
						<?php foreach ($food_data as $food_item) { ?>
							<div class = "timeline-item">
								<div class = "timeline-image" style = "background-image: url('<?php echo get_stylesheet_directory_uri() ?>/images/map_images/<?php echo $food_item->large_image_name ?>')"></div>
								<div class = "timeline-info-container">
									<h4><?php echo $food_item->food_name ?></h4>
									<div class = "mini-divider"></div>
									<p><?php echo $food_item->description ?></p>
									<!-- <h5>Location:</h5>
									<p><?php foreach ( $food_item->countries as $country ) { echo $country.' '; } ?></p> -->
								</div>
							</div>
						<?php } ?>
					</div>
					<div class = "pagination-tray">
						<div class = "previous-button"></div>
						<div class = "next-button"></div>
					</div>
				</section>
			<?php break;
			case 'dual_column_bullet_lists': ?>
				<section class="module-dual-column-bullet-lists">
					<div class="inner-container">
						<div class="single-column">
							<span class="column-title">
								<img src="<?php the_sub_field('left_column_image'); ?>" alt="<?php the_sub_field('left_column_title'); ?>">
								<?php the_sub_field('left_column_title'); ?>
							</span>
							<ul>
								<?php foreach( ['left_column_list_item_one', 'left_column_list_item_two', 'left_column_list_item_three', 'left_column_list_item_four', 'left_column_list_item_five'] as $field ) { ?>
									<?php if (get_sub_field($field)){ ?>
										<li><?php the_sub_field($field); ?></li>
									<?php } ?>
								<?php } ?>
							</ul>
						</div>

						<div class="single-column">
							<span class="column-title">
								<img src="<?php the_sub_field('right_column_image'); ?>" alt="<?php the_sub_field('right_column_title'); ?>">
								<?php the_sub_field('right_column_title'); ?>
							</span>
							<ul>
								<?php foreach( ['right_column_list_item_one', 'right_column_list_item_two', 'right_column_list_item_three', 'right_column_list_item_four', 'right_column_list_item_five'] as $field ) { ?>
									<?php if (get_sub_field($field)){ ?>
										<li><?php the_sub_field($field); ?></li>
									<?php } ?>
								<?php } ?>
							</ul>
						</div>
					</div>
				</section>
			<?php break;
		}

	endwhile;
endif; //end flexible content
?>

<section class="module-violator">
	<div class="inner-container">
		
			<span class="module-violator-pre-logo-text">Brought to you by</span>
			<span class="tetrapak-logo tetrapak-logo-white"></span>
			<span class="module-violator-arrow mobile-only-global"></span>
			<p>We make food & beverage cartons that protect what's good by keeping the food inside them safe and sound. 
			Recyclable and made with renewable materials, every single carton is part of our groundwork for a shared, 
			healthier future.<a class="about-tetra">Learn more</a></p>
		
	</div>
</section>

<?php if ( get_field('next_stories') ):
	$next_stories = get_field('next_stories'); ?>
	<section class="module-next-article">
		<div class="module-next-article-background"></div>
		<h3>Explore further</h3>
		<div id="next-article-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
			<div class="carousel-inner" role="listbox">
				<?php $first = true; foreach( $next_stories as $post ) : ?>
					<?php setup_postdata($post); ?>
					<div class="item <?php if ( $first ) echo 'active'; $first = false; ?>">
						<div class="img-container" style = "background-image: url('<?php echo esc_url(the_field('hero_image', $post['story']->ID));?>')">
						</div>
						<div class="carousel-caption">
							<h5><?php the_field('category', $post['story']->ID); ?></h5>
							<h4>
								<a href="<?php the_permalink($post[story]->ID); ?>">
									<?php echo $post['story']->post_title; ?>
								</a>
							</h4>
						</div>
					</div>

				<?php endforeach; ?>
			</div>

			<a class="left carousel-control" href="#next-article-carousel" role="button" data-slide="prev"></a>
			<a class="right carousel-control" href="#next-article-carousel" role="button" data-slide="next"></a>
		</div>
	</section>
<?php endif; ?>











	<?php endwhile; endif; ?>

<section id = "share-bar">
	<div class = "share-tray">
		<div class = "credit-content">
			<h5>Brought to you by:</h5>
			<a class = "tetrapak-logo about" href = "#"></a><br>
		</div>
		<div class = "social-links">
			<a class = "fb share social-icon" href = "#"></a><a class = "twitter share social-icon" href = "#"></a>
		</div>
	</div>
</section>

<script type="text/javascript">
	var templateDir = "<?php bloginfo('template_directory') ?>";
</script>

<?php get_footer(); ?>