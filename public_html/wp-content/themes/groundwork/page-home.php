<?php 
/*
Template Name: Homepage
*/

get_header(); 

if ( have_posts() ) : while ( have_posts() ) : the_post(); 
?>


<section class = "homepage-hero">
		
		<div class = "hero-container">
			<h1>Ideas that change how<br> we eat, think & live</h1>
			<div class = "tetra-animation-container">
				<video autoplay loop poster = "images/videostill.png">
		    		<source src="/wp-content/themes/groundwork/video/loopingv.mp4" poster = "/wp-content/themes/groundwork/video/fallback.png" type="video/mp4">
		    		<source src="/wp-content/themes/groundwork/video/loopingv.ogv" poster = "/wp-content/themes/groundwork/video/fallback.png" type="video/ogv">
		    		<source src="/wp-content/themes/groundwork/video/loopingv.webm" poster = "/wp-content/themes/groundwork/video/fallback.png" type="video/webm">
					<img src="/wp-content/themes/groundwork/video/fallback.png"> 
				</video>
				<img src = "/wp-content/themes/groundwork/images/hero_graphic_mask_sm.png">
			</div>
			
			<div class = "groundwork-info-container">
				<p>All over the world, people are working in ways big and small on the things that matter most, from the environment around us to the food that fuels us.</p>
				<p>With Groundwork, we're sharing those stories, and we're making it easy for you to do the same.</p>
				<!-- <a class = "read-more-button" href = "#">Read More</a> -->
			</div>
			<div class = "vertical-explore-arrow-container">
				<span>Explore</span>
				<div class = "vertical-explore-arrow"></div>
			</div>
		</div>
		
</section>


<?php 
	
	$i = 0;
	if ( get_field('homepage_stories') ): 
		$homepage_posts = get_field('homepage_stories');
		foreach( $homepage_posts as $post ) : ?>


		<?php setup_postdata($post); ?>
			
			<?php if ( $i % 3 == 0) { ?>
				
				<section class="full-post" style="background: transparent url('<?php the_field("hero_image", $post[story]->ID ); ?>'); background-size: cover; background-position:center center">
					<div class="wrapper">
						<div class="row">
							<div class="content-card col-7 push-1">
								<div class="row">
									<div class="content-card__content col-10 push-1">
										<h3><?php the_field('category', $post[story]->ID ); ?></h3>
										<h1><a href="<?php the_permalink($post[story]->ID); ?>"><?php echo $post[story]->post_title; ?></a></h1>
										<h2><?php the_field('summary', $post[story]->ID ); ?></h2>
										<div class="content-card__read"><a href="<?php the_permalink($post[story]->ID); ?>">Read more</a></div>	
									</div>
								</div>
							</div>
						</div>
					</div>
				</section> 

			<?php } ?>

			<?php if ( $i % 3 == 1) { ?>

				<section class="split-post right">
					<div class="wrapper">
						<div class="row tablet_row">
							<?php if (get_field("custom_hero_percentage", $post[story]->ID )) { $custom_hero_percentage = get_field("custom_hero_percentage", $post[story]->ID ); } else { $custom_hero_percentage = "center"; } ;?>
							<div class="split-post__img col-6" style="background: transparent url('<?php the_field("hero_image", $post[story]->ID ); ?>'); background-size: cover; background-position: <?php echo $custom_hero_percentage; ?> center"> 
							</div>
							<div class="content-card col-7 pull-2">
								<div class="row">
									<div class="content-card__content col-10 push-1">
										<h3><?php the_field('category', $post[story]->ID ); ?></h3>
										<h1><a href="<?php the_permalink($post[story]->ID); ?>"><?php echo $post[story]->post_title; ?></a></h1>
										<h2><?php the_field('summary', $post[story]->ID ); ?></h2>
										<div class="content-card__read"><a href="<?php the_permalink($post[story]->ID); ?>">Read more</a></div>	
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			<?php } ?>

			<?php if ( $i % 3 == 2) { ?>

				<section class="split-post left">
					<div class="wrapper">
						<div class="row tablet_row">
							<?php if (get_field("custom_hero_percentage", $post[story]->ID )) { $custom_hero_percentage = get_field("custom_hero_percentage", $post[story]->ID ); } else { $custom_hero_percentage = "center"; } ;?>
							<div class="content-card col-7 push-1">
								<div class="row">
									<div class="content-card__content col-10 push-1">
										<h3><?php the_field('category', $post[story]->ID ); ?></h3>
										<h1><a href="<?php the_permalink($post[story]->ID); ?>"><?php echo $post[story]->post_title; ?></a></h1>
										<h2><?php the_field('summary', $post[story]->ID ); ?></h2>
										<div class="content-card__read"><a href="<?php the_permalink($post[story]->ID); ?>">Read more</a></div>
									</div>	
								</div>
							</div>
							<div class="split-post__img col-6 pull-2" style="background: transparent url('<?php the_field("hero_image", $post[story]->ID ); ?>'); background-size: cover; background-position: <?php echo $custom_hero_percentage; ?> center"> 							
							</div>
						</div>
					</div>
				</section>

			<?php } ?>

		<?php $i++; endforeach; ?>
		<?php wp_reset_postdata(); ?>

<?php endif; ?>

<?php 
endwhile;
endif;
?>


<?php get_footer(); ?>