<?php wp_footer(); ?>

<footer id="page-footer">
  <div class="inner-container">
    <div class="footer-section">
      <p class="groundwork-footer">
       <?php echo file_get_contents(get_stylesheet_directory()."/images/groundwork_logo_nav.svg"); ?>
        <br class="mobile-only-global">
        Groundwork features stories about the way we raise, consume, and think about food, and how we care for ourselves and our resources in the process.
      </p>
    </div>

    <div class="footer-section">
      <p class="tetrapak-footer">
        <img width="118" height="118" src="<?php echo get_stylesheet_directory_uri(); ?>/images/tetrapak_about_logo.svg" alt="tetrapak logo">
        <br class="mobile-only-global">
        Protecting what’s good means protecting food, people, and the future of our planet. It’s what Tetra Pak, and our cartons, have always committed to do. With Groundwork, we’re proud to celebrate the efforts of those who share in that mission.
      </p>
    </div>
  </div>
</footer>
  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WNTXG2"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-WNTXG2');</script>
  <!-- End Google Tag Manager -->
</body>
</html>