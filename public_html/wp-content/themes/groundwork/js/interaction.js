jQuery(function($) {
	$(document).ready(function()  {

		// Make all external links open in a new tab
		$('a').not('[href*="mailto:"]').each(function () {
			var isInternalLink = new RegExp('/' + window.location.host + '/');
			if ( ! isInternalLink.test(this.href) ) {
				$(this).attr('target', '_blank');
			}
		});
		
		// If we're on a 'story' page...
		if ($('body').hasClass('single-post')){
			var prevScrollTop = 0;
			$(window).scroll(function(event){
	   			var currentScrollTop = $(this).scrollTop();
	   			if (currentScrollTop > prevScrollTop){
	       			$('#share-bar').removeClass('active');
	   			} else {
	      			$('#share-bar').addClass('active');
	   			}
	   			prevScrollTop = currentScrollTop;
			});

			// Open facebook sharer window when share on fb link is clicked
			$('.social-icon.fb.share').click(function(e){
				e.preventDefault();
				var encodedString = encodeURIComponent(window.location.href);
				var url = "https://www.facebook.com/sharer/sharer.php?m2w&s=100&u="+encodedString; 
				var windowName = "Share This Story";
				var left = (screen.width/2)-350;
				var top = (screen.height/2)-200;
				var windowSize = "width=600,height=257,top="+top+",left="+left;
				window.open(url, windowName, windowSize);
			});

			$('.social-icon.twitter.share').click(function(e){
				e.preventDefault();
				var thisUrl = encodeURIComponent(window.location.href);
				var url = "http://twitter.com/share?url=" + thisUrl;
				var windowName = "Share This Story";
				var left = (screen.width/2)-350;
				var top = (screen.height/2)-200;
				var windowSize = "width=765,height=500,top="+top+",left="+left;
				window.open(url, windowName, windowSize);
			});

			$('.module-map').on('click', '[data-countries]', function(event) {
				var link, countries, foodName, image, modalWindow;
				link = $(this);
				foodName = $.trim(link.text());
				countries = $.trim(link.data('countries')).split(' ');
				image = 'url(' + link.data('image') + ')';

				// Highlight countries
				$('.module-map svg .active').removeClass('active');
				$.each(countries, function(index, country) {
					$('.module-map svg #' + country).addClass('active');
				});

				// Highlight the current food item
				$('.module-map .module-map-food-name.active').removeClass('active');
				$(this).addClass('active');

				modalWindow = $('#map-modal');
				if ( modalWindow.hasClass('in') ) {
					modalWindow.modal('hide');
				}
				setTimeout(function() {
					// Fill the modal with data
					$('#map-modal h3').html(foodName);
					$('#map-modal .map-modal-description').html(link.data('description'));
					$('#map-modal .map-modal-countries').html(countries.join(', ').replace(/_/g, ' ')).attr('data-food-name', foodName);
					$('#map-modal .map-modal-image').css('background-image', image);

					modalWindow.modal('show');
				}, 500);
			});


			// $('#map-modal').on('hide.bs.modal', function() {
			// 	$('.module-map svg .active').removeClass('active');
			// 	$('.module-map .module-map-food-name.active').removeClass('active');
			// })

			$('.timeline-container').on('init', function(event, slick){

	        	$('.slick-current').prev().addClass('prev');
	        	$('.slick-current').next().addClass('next');      	
        	})

        	$('.timeline-container').on('beforeChange', function(event, slick, currentSlide, nextSlide){
           		var direction = (nextSlide - currentSlide);
           		if (direction > 0){
           			$('.slick-current').addClass('prev');
           		} else if (direction < 0 ) {
           			$('.slick-current').addClass('next');
           		}    
        	})

        	$('.timeline-container').on('afterChange', function(event, slick, currentSlide, nextSlide){
  	
	            $('.slick-current').prev().addClass('prev');
	        	$('.slick-current').next().addClass('next');
	 			$('.slick-current').prev().prev().removeClass('prev next');
	 			$('.slick-current').next().next().removeClass('next prev');
	        });

	        // Find the number of items so we can choose the center item as the starting slide.
	        var numberOfItems = $('div').filter('.timeline-item').length;
	        var startingSlide = Math.floor(numberOfItems/2);
			$('.timeline-container').slick({
	    		variableWidth: true,
	    		centerMode: true,
	    		arrows: false,
	    		infinite: false,
	    		initialSlide: startingSlide
	  		});

	  		$('.previous-button').click(function(){
	    		$(".timeline-container").slick('slickPrev');
			});
			$('.next-button').click(function(){
	    		$(".timeline-container").slick('slickNext');
			});

			$('.module-violator').on('click', function(event) {
				if ( $(event.target).closest('p').length === 0 ) {
					$(this).toggleClass('active');
				}
			});

			$('.carousel').bcSwipe({ threshold: 0 });
		}

		// SVG PROGRESS BAR SCRIPT 
		
		window.randomize = function() {
	  	
		  	// var rand = Math.floor(Math.random() * 100);
			var x = document.querySelector('.progress-circle-prog');
		    	x.style.strokeDasharray = (90 * 4.15) + ' 999';
			var el = document.querySelector('.progress-text'); 
			var from = $('.progress-text').data('progress');
			$('.progress-text').data('progress', 90);
			var start = new Date().getTime();
	  
			setTimeout(function() {
			    var now = (new Date().getTime()) - start;
			    var progress = now / 700;
				  	result = 90 > from ? Math.floor((90 - from) * progress + from) : Math.floor(from - (from - 90) * progress);
			    el.innerHTML = progress < 1 ? result+'%' : 90+'%';
			    if (progress < 1) setTimeout(arguments.callee, 10);
			}, 10);
		}

		if ( $(".interactive-pieces").length ){
			$('.html-block').waypoint( function(direction) {
			  
			  if(direction == "down") {

			  	window.randomize();
			  	$(".map-container").addClass("active");
			  	$(".progress-circle-container").removeClass("active");
			  	$(".gauge > .container").addClass("active");
			  	$(".clock-container").addClass("active");


			  } else if(direction == "up") {

				$(".map-container").removeClass("active");	
				$(".progress-circle-container").addClass("active");
				$(".gauge > .container").removeClass("active");				    
				$(".clock-container").removeClass("active");

			  }

			},{offset: '70%'});
		}

		// END_SVG PROGRESS BAR SCRIPT 

		$(".domestic, .industrial, .food").click(function(){


			if ( $(this).hasClass("active") ){

				$(".mobile-only").removeClass("active");
				$(this).removeClass("active");
				

			} else {

				$(".domestic, .industrial, .food").removeClass("active");
				$(".mobile-only").removeClass("active");
				$(this).toggleClass("active");
				$(this).next().toggleClass("active");

			}
			
		});
		

		// water drop interaction
		var waterGallonImg = $(".image-container__water");
			
		
		function desktopWaterDrop() {

			if ( $(window).width() > 965 ){
				waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/desktop/water_01.png");	


				$(".menu > .col-4").click(function(){

					$('div[class*="image-container__description"]').fadeOut();

					if( $(this).hasClass("domestic") ){

						waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/desktop/water_02.png");			
			
						setTimeout(function(){
							$(".image-container__description-1").fadeIn();
						}, 300);

					}
					else if ( $(this).hasClass("industrial") ){

						waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/desktop/water_03.png");			
					
						setTimeout(function(){
							$(".image-container__description-2").fadeIn();
						}, 300);

					} 
					else if ( $(this).hasClass("food") ){

						waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/desktop/water_04.png");			
						
						setTimeout(function(){
							$(".image-container__description-3").fadeIn();
						}, 300);

					} 
					
				});

				
				$(".menu > .col-4").on('mouseenter', function(){

					var hold = $(".water-drop .menu .col-4").hasClass("active");
					
					if(!hold){
						if( $(this).hasClass("domestic") ){

							waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/desktop/water_02.png");		
				
							setTimeout(function(){
								$(".image-container__description-1").fadeIn();
							}, 300);

						}
						else if ( $(this).hasClass("industrial") ){

							waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/desktop/water_03.png");		
						

							setTimeout(function(){
								$(".image-container__description-2").fadeIn();
							}, 300);

						} 
						else if ( $(this).hasClass("food") ){

							waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/desktop/water_04.png");		
							
							setTimeout(function(){
								$(".image-container__description-3").fadeIn();
							}, 300);

						} 

					}
				}).on('mouseleave', function(){

					var hold = $(".water-drop .menu .col-4").hasClass("active");

					if(!hold){
						waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/desktop/water_01.png");		
							
						setTimeout(function(){
							$("[class^='image-container__description']").fadeOut();
						}, 100);
						
					}

				});		
				
			} else {

				waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/tablet/TP_RenewableLiving_Tablet_infographic2_01.png");		
				tabletWaterDrop(); 
			}

		}

		desktopWaterDrop();


		function tabletWaterDrop() {
			// 965 is same with 980(tabelt-breakpoint)
			if ( $(window).width() <= 965 ){	

				$(".menu > .col-4").click(function(){

					$('div[class*="image-container__description"]').fadeOut();

					if( $(this).hasClass("domestic") ){

						waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/tablet/TP_RenewableLiving_Tablet_infographic2_02.png");			
			
						setTimeout(function(){
							$(".image-container__description-1").fadeIn();
						}, 300);

					}
					else if ( $(this).hasClass("industrial") ){

						waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/tablet/TP_RenewableLiving_Tablet_infographic2_03.png");			
					
						setTimeout(function(){
							$(".image-container__description-2").fadeIn();
						}, 300);

					} 
					else if ( $(this).hasClass("food") ){

						waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/tablet/TP_RenewableLiving_Tablet_infographic2_04.png");			
						
						setTimeout(function(){
							$(".image-container__description-3").fadeIn();
						}, 300);

					} 
					
				});



				$(".menu > .col-4").on('mouseenter', function(){

					var hold = $(".water-drop .menu .col-4").hasClass("active");
					
					if(!hold){
						if( $(this).hasClass("domestic") ){

							waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/tablet/TP_RenewableLiving_Tablet_infographic2_02.png");		
				
							setTimeout(function(){
								$(".image-container__description-1").fadeIn();
							}, 300);

						}
						else if ( $(this).hasClass("industrial") ){

							waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/tablet/TP_RenewableLiving_Tablet_infographic2_03.png");		
						

							setTimeout(function(){
								$(".image-container__description-2").fadeIn();
							}, 300);

						} 
						else if ( $(this).hasClass("food") ){

							waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/tablet/TP_RenewableLiving_Tablet_infographic2_04.png");		
							
							setTimeout(function(){
								$(".image-container__description-3").fadeIn();
							}, 300);

						} 

					}
				}).on('mouseleave', function(){

					var hold = $(".water-drop .menu .col-4").hasClass("active");

					if(!hold){
						waterGallonImg.attr('src', "/wp-content/themes/groundwork/images/water/tablet/TP_RenewableLiving_Tablet_infographic2_01.png");		
							
						setTimeout(function(){
							$("[class^='image-container__description']").fadeOut();
						}, 100);
						
					}

				});		

			}
		}

		tabletWaterDrop(); 

		$(window).on('resize', function(){
			 
			desktopWaterDrop();

		});


		// END_water drop interaction

		var current = [
      
	      ["",40],
	      ["",40],
	      ["",40] 

	    ];

	    var fossil = [

	      ["COAL",38],
	      ["OIL",33],
	      ["GAS",31]  

	    ];

	    var minerals = [

	      ["ALUMINUM",76],
	      ["COPPER",28],
	      ["SILVER",13]

	    ];

	    var ecosystems = [

	      ["RAINFOREST",192],
	      ["COAL REEFS",84],
	      ["FARMLAND",65]

	    ];

	    
	    var chart = document.getElementById("chart"),
	        axisMargin = 20,
	        margin = 20,
	        valueMargin = 4,
	        width = 1080 - margin*2
	        height = 385 - margin*2;
	        barHeight = 2,
	        barPadding = 83,
	        labelWidth = 0;

	    max = d3.max(current.map(function(i){ 
	      return i[1];
	    }));

	    maxRoundUp = Math.ceil(max/10) * 10;

	    svg = d3.select(chart)
	      .append("svg")
	      .attr("width", width + margin*2 + 85)
	      .attr("height", height + margin*2)
	      .attr("viewBox", "0 0 1165 385")
      	  .attr("preserveAspectRatio","xMinYMid")
      	  .attr("id","line");

	    bar = svg.selectAll("g")
	      .data(current)
	      .enter()
	      .append("g");

	    bar.attr("class", "bar")
	      .attr("cx",0)
	      .attr("transform", function(d, i) { 
	         return "translate(" + margin + "," + (i * (barHeight + barPadding) + barPadding) + ")";
	      });

	    // y axis, labels
	    var label = bar.append("text")
	      .attr("class", "label")
	      .attr("x", 140 - margin )
	      .attr("y", barHeight / 2)
	      .attr("dy", ".25em") //vertical align middle
	      .text(function(d){
	        return d[0];
	      }).each(function() {
	        labelWidth = Math.ceil(Math.max(labelWidth, this.getBBox().width));
	      })
	      .attr("text-anchor","end");
	
	    scale = d3.scale.linear()
	      .domain([0, maxRoundUp])
	      .range([0, width - margin*2 - 85]);

	    xAxis = d3.svg.axis()
	      .scale(scale)
	      .orient("bottom")
	      .tickFormat(function(d) { 
	      
	        return d+"yrs";

	      })
	      .outerTickSize(0);

	    d3.selectAll(".axis .tick text").attr("y", 20);
	     
	    y = d3.scale.linear().range([height-20, 0]);

	    yAxis = d3.svg.axis().scale(y)
	        .orient("left")
	        .ticks(0)
	        .outerTickSize(0);

	    bar.append("rect")
	       .attr("transform", "translate("+140+", 0)")
	       .attr("height", barHeight)
	       .attr("width", function(d){
	          return 60;
	        });

	    var circle = bar.append("g")
	            .attr("transform", function(d,i){
	                 return "translate(200,0)";
	              });
	     
	    circle.append("circle")
	      .attr("class","circle")
	      .attr("r", function(d){return 12} );             

	    circle.append("text")
	      .attr("class","result")
	        .attr("dx", function(d){return -7})
	        .attr("dy", ".35em") 
	      .text(function(d){  return ""; })
	      
	    var a = svg.insert("g",":first-child")
	     .attr("class", "axis")
	     .attr("transform", "translate(" + (margin + 140) + ","+ (height-10)+")")
	     .call(xAxis);

	    a.selectAll("text")
	     .attr("y", "20");
	     

	    a.selectAll("line")
	      .attr("y2","12")
	      .attr("y1", "-1");

	    svg.append("g")
	         .attr("class", "y axis")
	         .attr("transform", "translate(" + (margin + 140) + "," + margin +")")
	         .call(yAxis);

	    $(".axis > .tick:first-child > text").html("PRESENT"); 

	     var line = $("#line"),
	       	 aspect = line.width() / line.height(),
        	 container  = line.parent();


	    $(window).on("resize", function(){
	      if($(window).width() > 964){
	        
	        var targetWidth = container.width();
	        line.attr("width", targetWidth);
	        line.attr("height", Math.round(targetWidth/aspect));

	      } else if( $(window).width() <= 964 ) {

	      	  line.attr("width", "805");
	          line.attr("height", "265");

	      }

	    }).trigger("resize");

		
	    $('.line-graph .menu .col-4').on('click', function(){

	    	if($(this).hasClass("active")){
	    	
	    		$(this).removeClass("active");	
	    		
	    		if( $(window).width() < 753){
	    			$(".line-graph__mobile--container").slideUp(300);
	    		}


	    	} else {


	    		$(".line-graph__mobile--container").slideUp(300);


	    		$('.line-graph .menu .col-4').removeClass("active");	

		    	if ( $(this).attr("data") == "fossil" ){
			      	
			      	$(this).addClass("active");
			    
			      	if( $(window).width() < 753){
			      		$(this).next().slideDown(300);
			      	}

			    	dataUpdate(fossil);
			    	$(".bar:nth-child(2) > g > text").attr("dx","-7"); 
			    	
			    } else if ( $(this).attr("data") == "minerals" ){ 

					$(this).addClass("active");

					if( $(window).width() < 753){
			      		$(this).next().slideDown(300);
			      	}


					dataUpdate(minerals);	      	
					$(".bar:nth-child(2) > g > text").attr("dx","-7"); 
					
			    } else if ( $(this).attr("data") == "ecosystems" ){

			    	$(this).addClass("active");

			    	if( $(window).width() < 753){
			      		$(this).next().slideDown(300);
			      	}


			      	dataUpdate(ecosystems);	
			        $(".bar:nth-child(2) > g > text").attr("dx","-10.5"); 
			        $(".axis > .tick:first-child > text").html("PRESENT");	      

			    }	

	    	}

	    });



       	if( $(window).width() > 752 ){
	    
	    $('.line-graph .menu .col-4').on('mouseenter', function(){

	      var holdCondition = $('.line-graph .menu .col-4').hasClass("active");
 
	      if ( !holdCondition ){		

		      if ( $(this).attr("data") == "fossil" ){
		      	
		      	dataUpdate(fossil);

		      } else if ( $(this).attr("data") == "minerals" ){ 

				dataUpdate(minerals);	      	
		  
		      } else if ( $(this).attr("data") == "ecosystems" ){

		      	dataUpdate(ecosystems);	

		        $(".bar:nth-child(2) > g > text").attr("dx","-10.5"); 
		        $(".axis > .tick:first-child > text").html("PRESENT");	      
		      }

	  	  }
	    }).on('mouseleave', function(){

	    	var holdCondition = $('.line-graph .menu .col-4').hasClass("active");

	  		if ( !holdCondition ){	

	        // 1) initialize y axis label 
			label.data(current).transition().duration(150).style("opacity",0).transition().duration(150).style("opacity","1").text(function(d){ return d[0]});	
	        
	        // bar.select(".label").data(current).text(function(d){ return d[0];})
	          
	        // 2) initialize y axis line    
	        bar.select("rect").transition().duration(700).attr("width", function(d){ return 60;});

	        // 3) initialize y axis ball
	        bar.select("g").transition().duration(700).attr("transform", function(d,i){ return "translate(200,0)";})

	        // 4) initialize y axis text in the ball    
	        bar.select(".result").data(current).text(function(d){return "";})

	        // 5) initialize x axis range
	        var max = d3.max(current.map(function(i){ return i[1];}));
	        var maxRoundUp = Math.ceil(max/10) * 10;

	        scale = d3.scale.linear().domain([0, maxRoundUp]).range([0, width - margin*2 - 85]);

	        xAxis = d3.svg.axis().scale(scale).orient("bottom").tickFormat(function(d) { if( d == 0 ){ return "PRESENT"; }return d+"yrs"; })
	        .outerTickSize(0);            

	        svg.select(".axis").transition().duration(700).call(xAxis);

	        var a = svg.select(".axis")
	           .transition()
	           .duration(700)
	           .call(xAxis);    

	        a.selectAll("text")
	        .attr("y", "20");

	        a.selectAll("line")
	        .attr("y2","12");  

	        $(".bar:nth-child(2) > g > text").attr("dx","-7");    

	    	}

	    });

		}


		var dataUpdate = function(data){

			// 5) update x axis range
	        var max = d3.max(data.map(function(i){ return i[1];}));
	        var maxRoundUp = Math.ceil(max/10) * 10;

	        scale = d3.scale.linear()
	                  .domain([0, maxRoundUp])
	                  .range([0, width - margin*2 - 85]);

	        xAxis = d3.svg.axis()
	        .scale(scale)
	        .orient("bottom")
	        .tickFormat(function(d,i) { 

	            if( d == 0 ){
	              return "PRESENT";
	            } 
	            return d+"yrs";

	        })
	        .outerTickSize(0);            

	        var a = svg.select(".axis")
	           .transition()
	           .duration(700)
	           .call(xAxis);    

	        a.selectAll("text")
	        .attr("y", "20");

	        a.selectAll("line")
	        .attr("y2","12")
	        .attr("y1", "-1");  
	        
	        // 1) update y axis label 
			label.data(data)
				.transition().duration(150)
				.style("opacity",0)
				.transition().duration(150)
				.style("opacity","1")
				.text(function(d){ return d[0]});

	          
	        // 2) update y axis line    
	        bar.select("rect")
	          .data(data)
	          .transition()
	          .duration(700)
	          .attr("width", function(d){
	            return scale(d[1]);
	          });

	        // 3) update y axis ball
	        bar.select("g")
	            .data(data)
	            .transition()
	            .duration(700)
	            .attr("transform", function(d,i){
	              return "translate("+ (scale(d[1]) + 130) +  "," + "0)";
	            })

	        // 4) update y axis text in the ball    
	        bar.select(".result")
	            .data(data)
	            .text(function(d){
	              return d[1];
	            })

		}

	

		$(window).on('resize', function(){

			

		});

	
		
		

	    


		// Line graph interaction by d3.js
		// Set the dimensions of the canvas / graph / data



		// END_Line graph interaction by d3.js

		$('.page-nav .about, .tetrapak-logo.about, .about-tetra').on('click', function(event) {
			$('body').removeClass('grid-nav-content-visible');
			$('.grid-button-container').removeClass('active');
			var scrollTopPosition = $(window).scrollTop();
			$('body').toggleClass('page-sub-nav-visible');

			if ( $('body').hasClass('page-sub-nav-visible') ) {
				$('.page-sub-nav a:first').click();
				$('body').data('scroll-top-before-about-page-show', scrollTopPosition);
				$("html, body").scrollTop(0);
			} else {
				$('.page-sub-nav .active, .about-tab-content .active').removeClass('active');
				$("html, body").scrollTop($('body').data('scroll-top-before-about-page-show'));
			}

			return false;
		});

		$('.page-nav .grid-button-container').on('click', function(event) {
			$('body').toggleClass('grid-nav-content-visible');
			$(this).toggleClass('active');
			if ( $('#share-bar').hasClass('active') ) {
				$('#share-bar').removeClass('active');
			}

			$('body').removeClass('page-sub-nav-visible');

			if ( $('body').hasClass('page-sub-nav-visible') ) {
				$('.page-sub-nav a:first').click();
				$('body').data('scroll-top-before-about-page-show', scrollTopPosition);
				$("html, body").scrollTop(0);
			} else {
				$('.page-sub-nav .active, .about-tab-content .active').removeClass('active');
				$("html, body").scrollTop($('body').data('scroll-top-before-about-page-show'));
			}


			return false;
		});

		$('.grid-nav-content').on('click', function(event) {
			if ( $(event.target).closest('.grid-nav-content-inner-background').length === 0 ) {
				$('body').removeClass('grid-nav-content-visible');
				$('.grid-button-container').removeClass('active');
			}
		});

		$(document).on('keyup', function(event) {
			if ( event.keyCode === 27 ) { // on ESC
				if ( $('body').hasClass('grid-nav-content-visible') ) {
					$('body').removeClass('grid-nav-content-visible');
					return;
				}

				if ($('body').hasClass('page-sub-nav-visible')) {
					$('.page-nav .about').trigger('click');
				}
			}
		});

		$('.page-sub-nav').on('show.bs.tab', '[data-toggle="tab"]', function(event) {
			$("html, body").scrollTop(0);
		});

		$('.page-nav .logo').addClass('loaded');
		$('.page-nav .logo').on('mouseenter', function(event) {
			var logo = $(this);
			logo.removeClass('loaded');
			setTimeout(function() { logo.addClass('loaded'); }, 100);
		});		
	}); // End document.ready()
}); // End JQuery(function($)){}

